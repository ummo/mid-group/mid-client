package xyz.ummo.mid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import xyz.ummo.mid.ui.main.MainFragment
import xyz.ummo.mid.ui.main.MainViewModel

class MainActivity : AppCompatActivity() {
    val vm: MainViewModel get() = ViewModelProviders.of(this).get(MainViewModel::class.java)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }

    }

}
