package xyz.ummo.mid
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

class GenericViewHolder(private val binding: ViewDataBinding)  :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(obj: Object) {
        binding.setVariable(BR.obj, obj)
        binding.executePendingBindings()
    }
}