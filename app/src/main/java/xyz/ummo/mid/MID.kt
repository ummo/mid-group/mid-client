package xyz.ummo.mid

import android.app.Application
import com.github.kittinunf.fuel.core.FuelManager
import xyz.ummo.mid.R

class MID: Application() {
    override fun onCreate() {
        super.onCreate()
        FuelManager.instance.basePath = getString(R.string.questions_url)
        FuelManager.instance.timeoutReadInMillisecond = 60000
    }
}