package xyz.ummo.mid

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView

val TAG = "QuestionsAdapter"
class QuestionAdapter: RecyclerView.Adapter<GenericViewHolder>() {
    var list:MutableLiveData<List<Question>>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ViewDataBinding = DataBindingUtil.inflate(layoutInflater,R.layout.question_ly , parent, false)
        return GenericViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GenericViewHolder, position: Int) {
        holder.bind(list?.value?.get(position) as Object)
    }

    fun setItem(list: MutableLiveData<List<Question>>) {
        this.list = list
    }

    override fun getItemCount(): Int = list?.value?.size!!
}
