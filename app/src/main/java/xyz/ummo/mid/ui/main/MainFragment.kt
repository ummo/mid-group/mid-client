package xyz.ummo.mid.ui.main

import android.app.Activity
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.main_fragment.*
import xyz.ummo.mid.Question
import xyz.ummo.mid.QuestionAdapter
import xyz.ummo.mid.R
import xyz.ummo.mid.databinding.MainFragmentBinding

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.e("Questioins_URL",getString(R.string.questions_url))
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ViewModelProviders.of(this).get(MainViewModel::class.java).also{
            val vm = it
            it.questions.observe(this, Observer {
                viewPager2.adapter = QuestionAdapter().also {
                    it.setItem(vm.questions)
                }
            })
        }


    }

}
