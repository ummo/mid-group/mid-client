package xyz.ummo.mid.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.github.kittinunf.fuel.Fuel
import org.json.JSONArray
import org.json.JSONException
import xyz.ummo.mid.Question

class MainViewModel : ViewModel() {
    val questions: MutableLiveData<List<Question>> by lazy {
        MutableLiveData<List<Question>>().also {
            Log.e("MainVM", "Loading questions")
            loadQuestions()
        }
    }

    fun getUsers(): LiveData<List<Question>> {
        return questions
    }

    public fun saveQuestions(){

    }


    private fun loadQuestions() {
        Fuel.get("/question").response{request,response,result ->
            try {
                println(response)
                println(request)
                println(result)
                val ql = ArrayList<Question>()
                val qja = JSONArray(String(response.data))
                Log.e("MAinVM","Got in ${qja}")
                for (i in 0 .. qja.length()-1){
                    val qjo = qja.getJSONObject(i)
                    ql.add(Question(qjo.getString("question"),qjo.getString("_id")))
                }
                Log.e("MainVM","Questions Length ${ql.size}")
                questions.postValue(ql)
            }catch (e:Exception){
                Log.e("MainVM","Error ${e}")
            }

        }


    }
}
