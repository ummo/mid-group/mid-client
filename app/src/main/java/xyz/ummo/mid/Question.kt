package xyz.ummo.mid

import com.github.kittinunf.fuel.Fuel
import com.google.gson.Gson

class Question(val question: String, val id: String, var answer: String = "") {
    fun saveAnswer() {
        Fuel.post("http://192.168.100.201:3002/data")
            .jsonBody(Gson().toJson(this))
            .response { request, response, result ->
                println(request)
                println(response)
                println(result)
            }
    }
}